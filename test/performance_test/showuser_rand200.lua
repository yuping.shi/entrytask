local baseid = 10000
local file = io.open('users.csv', 'r')
local file_lines = {}
for line in file:lines() do
    table.insert(file_lines, line)
end

function setup(thread)
    local user_id = math.random(1,1000000)
    thread:set("uid", user_id)
end


function request()
    local rand_user_id = math.random(1,1000000)
    local cook = "tokenid="..string.sub(file_lines[rand_user_id], string.find(file_lines[rand_user_id], "[0-9a-z%-]+$"))
    headers = {}
    headers["Cookie"] = cook
    return wrk.format('GET', '/show_profile/', headers)
end

function response(status,headers,body)
    print(body)
end