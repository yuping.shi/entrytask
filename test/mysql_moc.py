# -*- encoding: utf-8 -*-

import pymysql
from connecttest import config
from tqdm import tqdm
import datetime
import csv

def insert_users():
    conn = pymysql.connect(**config)  # 连接数据库
    cursor = conn.cursor()
    create_time = str(datetime.datetime.now())
    for i in tqdm(range(1000000)):
        # 获取user表中的所有数据
        sql = 'INSERT INTO `websystem`.`users` (`username`, `nickname`, `password`, `user_picture`, `create_time`) \
                VALUES (%s, %s, %s, %s, %s);'
        # 执行sql语句
        username = 'user_' + str(i)
        nickname = str(i)
        password = '123456'
        user_picture = 'img/img1.png'
        cursor.execute(sql, (username, nickname, password, user_picture, create_time))
        # 提交
        conn.commit()

    conn.close()


def truncate():
    conn = pymysql.connect(**config)  # 连接数据库
    cursor = conn.cursor()
    cursor.execute('truncate table users;')
    conn.commit()
    conn.close()


def get_csv():
    import uuid
    conn = pymysql.connect(**config)  # 连接数据库
    cursor = conn.cursor()
    cursor.execute('select * from users;')
    result = cursor.fetchall()
    header = ['username', 'password', "tokenid"]
    with open('users.csv', mode='w') as f:
        for r in result:
            line = []
            line.append(r['username'].encode('utf-8'))
            line.append(r['password'].encode('utf-8'))
            line.append(uuid.uuid3(uuid.NAMESPACE_OID, str(r['username'])).__str__())
            f.write(','.join(line))
            f.write('\n')

    conn.commit()
    conn.close()


if __name__ == '__main__':
    # insert_users()
    # truncate()
    get_csv()
