# -*- coding: utf-8 -*-

import redis
import traceback
import pymysql

def is_redis_ok():
    r = redis.Redis()
    try:
        if r.ping():
            print 'redis connected successfully.'
            return True
    except Exception as e:
        traceback.print_exc()
    print 'redis failed to connect!'
    return False


def is_mysql_ok(config):
    try:
        conn = pymysql.connect(**config)  # 连接数据库
        cursor = conn.cursor()
        # 获取user表中的所有数据
        sql = 'select * from users limit 1'
        # 执行sql语句
        cursor.execute(sql)
        # 使用fetchall获取所有数据
        result = cursor.fetchall()
        # 提交
        conn.commit()
        print 'mysql connected successfully.'
        return True
    except Exception as e:
        traceback.print_exc()
    print 'mysql failed to connect!'
    return False

config = {
    'host': 'localhost',  # 本地地址
    'port': 3306,  # 端口
    'user': 'root',  # 数据库用户名
    'password': '',  # 数据库密码是
    'db': 'websystem',  # 数据库的名字
    'charset': 'utf8mb4',
    'cursorclass': pymysql.cursors.DictCursor,
    # 'use_unicode': False,
    'charset': 'utf8mb4'
}

if __name__ == '__main__':
    is_redis_ok()

    is_mysql_ok(config)
