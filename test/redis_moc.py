import redis
import csv
from tqdm import tqdm


def set_tokenid():
    r = redis.Redis()
    try:
        with open('users.csv', mode='r') as f:
            reader = csv.reader(f)
            for row in tqdm(reader):
                r.setex(row[2], 3600, row[0])
    except Exception as e:
        print e.message


def add_seq():
    with open('users_200.csv', mode='r') as fr:
        reader = csv.reader(fr)
        with open('users_200_seq.csv', mode='w') as fw:
            writer = csv.writer(fw)
            for seq, line in enumerate(reader):
                writer.writerow([seq]+line)


if __name__ == '__main__':
    set_tokenid()
    # add_seq()