

import redis
def is_redis_ok():
    r = redis.Redis()
    try:
        if r.ping():
            print 'redis connected successfully.'
            return True
    except Exception as e:
        traceback.print_exc()
    print 'redis failed to connect!'
    return False
