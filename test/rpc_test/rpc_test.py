from threading import Thread
from rpc_test.connection import Client
from rpc_test.official_rpcclient import RPCProxy
import requests


def getClient(host, port):
    c = Client((host, port), authkey=b'peekaboo')
    proxy = RPCProxy(c)
    return proxy


def rpc_process(*args, **kwargs):
    rpc_clinet = getClient('localhost', 17000)
    result = rpc_clinet.add(*args, **kwargs)
    print result


def my_test():
    thread_count = 3
    for i in range(thread_count):
        t = Thread(target=rpc_process, args=(2, 3))
        t.run()


def old_rpc_test():
    import csv
    def req_profile(*args, **kwargs):
        from EntryTask.rpc_client import RPCClient
        rpc = RPCClient()
        rpc.connect('localhost', 10010)
        res = rpc.show_profile(*args, **kwargs)
        print res

    thread_count = 100
    lines = []
    with open('../users_200.csv', mode='r') as f:
        reader = csv.reader(f)
        for line in reader:
            lines.append(line)

    for line in lines[:5]:
        t = Thread(target=req_profile, kwargs={'tokenid': line[2]})
        t.run()


old_rpc_test()
