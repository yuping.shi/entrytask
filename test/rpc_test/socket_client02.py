import socket
import time

sock = socket.socket()
sock.connect(('127.0.0.1', 9006))
cnt = 1
while True:
    print 'client02 recv: ', sock.recv(1024)
    sock.send(b'client 02, cnt %d' % cnt)
    print "client02 send 'server %d' to: " % cnt
    cnt += 1
    time.sleep(3)
