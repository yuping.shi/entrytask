# -*- encoding:utf-8 -*-

import socket
import time
import threading

sock = socket.socket()
sock.bind(('127.0.0.1', 9006))
sock.listen(5)


def socket_recv_handle(client_socket):
    while True:
        r = client_socket.recv(1024)
        print 'server recv: ', r, 'from', client_socket.getpeername()


def socket_send_handle(client_socket):
    cnt = 1
    while True:
        print(cnt)
        client_socket.send(b'server %d' % cnt)
        print "server send 'server %d' to: " % cnt, client_socket.getpeername()
        cnt += 1
        time.sleep(1)


while True:
    print '监听...'
    client_sock, client_addr = sock.accept()
    print 'server accepted.'
    # 使用多线程，对阻塞式socket进行读写分离，避免读取时候一直被阻塞
    threading.Thread(target=socket_send_handle, args=(client_sock,)).start()
    threading.Thread(target=socket_recv_handle, args=(client_sock,)).start()

