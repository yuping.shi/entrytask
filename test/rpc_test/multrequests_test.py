#encoding=utf-8

from threading import Thread, Lock
from multiprocessing import Pool
import requests
import time

success_access = 0
success_error = 0
faild_access = 0


def multy_request_test():
    import csv
    lines = []

    success_access_lock = Lock()
    success_error_lock = Lock()
    faild_access_lock = Lock()

    with open('../users_200.csv', mode='r') as f:
        reader = csv.reader(f)
        for line in reader:
            lines.append(line)

    def request_moc(tokenid):
        global success_access
        global success_error
        global faild_access
        resp = requests.get(url='http://localhost:8000/show_profile/', cookies={'tokenid': tokenid})
        # print resp.status_code, resp.text[:15], tokenid
        if resp.status_code == 200:
            if 'Error' not in resp.text[:15]:
                with success_access_lock:
                    success_access += 1
            else:
                with success_error_lock:
                    success_error += 1
        else:
            with faild_access_lock:
                faild_access += 1

    start_time = time.time()
    while True:
        for line in lines:
            t = Thread(target=request_moc, kwargs={'tokenid': line[2]})
            t.run()
        end_time = time.time()

        print "本次测试共历时", end_time - start_time, 's，统计数据如下：'
        print 'success_access: ', success_access
        print 'success_error: ', success_error
        print 'faild_access: ', faild_access


multy_request_test()
