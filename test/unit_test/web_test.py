import unittest
import requests
import json
import imageop


class TestWebService(unittest.TestCase):
    def login(self):
        url = 'http://localhost:9000/login/'
        date = {'username': "user_0", "password": 'e10adc3949ba59abbe56e057f20f883e'}
        r = requests.request(method='POST', url=url, data=date)
        result = json.loads(r.text.encode('utf-8'))
        assert result['status'] == 100

    def show_profile(self):
        url = 'http://localhost:9000/show_profile/'
        cookies = {"tokenid": "e63bd754-42df-3c10-87d4-5b92f723f520"}
        r = requests.request(method='POST', url=url, cookies=cookies)
        result = json.loads(r.text.encode('utf-8'))
        assert result['status'] == 100

    def update_user_picture(self):
        url = 'http://localhost:8000/update_user_picture/'
        cookies = {"tokenid": "e63bd754-42df-3c10-87d4-5b92f723f520"}
        file_name = "../../static/img/img1.png"
        file = {"picture": open(file_name, mode="rb")}
        r = requests.request(method='POST', url=url, cookies=cookies, files=file)
        result = json.loads(r.text.encode('utf-8'))
        assert result['status'] == 100

    def update_nickname(self):
        url = 'http://localhost:8000/update_nickname/'
        cookies = {"tokenid": "e63bd754-42df-3c10-87d4-5b92f723f520"}
        data = {"nickname": "wawa"}
        r = requests.request(method='POST', url=url, cookies=cookies, data=data)
        result = json.loads(r.text.encode('utf-8'))
        assert result['status'] == 100

    def logout(self):
        url = 'http://localhost:8000/logout/'
        cookies = {"tokenid": "e63bd754-42df-3c10-87d4-5b92f723f520"}
        r = requests.request(method='POST', url=url, cookies=cookies)
        result = json.loads(r.text.encode('utf-8'))
        assert result['status'] == 100

    def test_web_service(self):
        self.login()
        self.show_profile()
        self.update_user_picture()
        self.update_nickname()
        self.logout()


if __name__ == '__main__':
    unittest.main()