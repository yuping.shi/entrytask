# encoding=utf-8
import json
import socket
import struct

from conf.log import log


class RpcClient(object):
    """
    rpc客户端，实现上依赖socket，序列化采用json
    负责与rpc服务端交互，发送序列化数据给服务端并从服务端接受传回来的执行结果
    """

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self, host, port):
        self.sock.connect((host, port))

    def send(self, data):
        self.sock.sendall(data)

    def recv(self, length):
        return self.sock.recv(length)

    def close(self):
        return self.sock.close()

    def call(self, function, *args):
        try:
            d = {'name': function, 'args': args}
            requests = json.dumps(d).encode('utf-8')
            length = struct.pack('I', len(requests))
            self.send(length)
            self.send(requests)
            # log.debug("send success")
            length_pre = self.recv(4)
            length, = struct.unpack('I', length_pre)
            data = self.recv(length)
            response = json.loads(data, encoding='utf-8')
            # log.debug("recv success")
            return response
        except Exception as e:
            log.debug("req_%s fail| error_type=%s" % (function, e))
            # return my_rsp(Code.FAIL, "server error", None).__dict__
