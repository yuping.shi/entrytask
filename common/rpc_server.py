# encoding=utf-8
import json
import socket
import struct
import thread

from conf.log import log
from error import error


class rpc_server(object):
    """
    rpc服务提供者，负责注册服务，消息序列化，接受客户端的服务调用请求。
    同样是基于socket实现，序列化采用json
    """

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.funcs = {}

    def connect(self, host, port):
        self.sock.bind((host, port))
        self.sock.listen(0xfff)
        self.loop(self.sock)

    def register_method(self, function, name=None):
        if name is None:
            name = function.__name__
        self.funcs[name] = function

    def loop(self, sock):
        """
        rpc服务端循环监听客户请求
        :param sock:
        :return:
        """
        while True:
            conn, addr = self.sock.accept()
            try:
                # 采用多线程
                thread.start_new_thread(self.handle_conn, (conn,))
            except socket.error as e:
                log.info("connect error,addr is %s" % addr)

    def handle_conn(self, conn):
        """
        rpc调用逻辑
        :param conn: 服务端socket
        :return:
        """
        try:
            while True:
                length_prefix = conn.recv(4)
                if not length_prefix:
                    conn.close()
                    break
                length, = struct.unpack("I", length_prefix)
                # body = receive(conn, length)
                body = conn.recv(length)
                request = json.loads(body, encoding='utf-8')
                name = request['name']
                args = request['args']
                try:
                    res = self.funcs[name](*args)
                except Exception as ex:
                    res = {'status': error.ERROR_RPCSRV_EXECUTION, 'msg': "ERROR_RPCSRV_EXECUTION"}
                # if res is None:
                #     res = my_rsp(Code.PARAM_INVALID, "param is invalid", None)
                if res:
                    response = json.dumps(res).encode('utf-8')
                else:
                    response = ''
                length_prefix = struct.pack("I", len(response))
                conn.sendall(length_prefix)
                conn.sendall(response)
        except socket.error as e:
            print 'socket error: ' + str(e)
