# encoding=utf-8
import socket
import threading

from rpc_client import RpcClient
import time
from conf import conf
from conf.log import log


class RpcClientPool(object):
    """
    客户端连接池，维护一个rpc_client_list列表，该列表中有多个rpc client实例，提供连接复用功能
    采用信号量的方式，保证线程安全
    参数: host是rpc client监听ip
        port是监听的端口
        num是线程池中的可复用连接数
    """

    def __init__(self, host, port, num=50):
        self.rpc_client_list = []
        self.num = threading.Semaphore(num)
        for i in range(num):
            client = RpcClient()
            client.connect(host, port)
            self.rpc_client_list.append(client)

    def get_rpc_client(self):
        self.num.acquire()
        client = self.rpc_client_list.pop()
        return client

    def release_rpc_client(self, client):
        self.rpc_client_list.append(client)
        self.num.release()
