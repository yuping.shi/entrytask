# encoding=utf-8

from dao.sql_api import WebSystemSql
from dao.redis_api import redis_template
import uuid
from error import error
import json
import hashlib

sql_server = WebSystemSql()


def tokenid_check(func):
    """
    装饰器，进行参数检查
    :param func:
    :return:
    """

    def wapper(*args, **kwargs):
        try:
            if len(args) == 0:
                raise Exception("no tokenid exists")
            if type(args[0]) is not str:
                raise Exception()
            res = func(*args, **kwargs)
        except Exception as e:
            res = {'status': error.FAILD_TOKEN_CHECK, 'msg': e.message}
        return res

    return wapper


def verify_password(user, password):
    """
    校验用户密码，如果待校验密码与用户存储密码一致则，返回用户的不敏感信息
    :param user: 一个存储用户信息的字典类型数据
    :param password: 待校验密码，字符串
    :return:
    """
    result = user
    # 用户待校验密码加盐散列
    password_md5 = hashlib.md5(password.encode("utf-8"))
    password_md5.update(user["salt"].encode("utf-8"))
    password = password_md5.hexdigest()
    if result['password'] == password:
        # 生成用户的token
        tokenid = str(uuid.uuid3(uuid.NAMESPACE_OID, str(result['username']) + str(result['salt'])))
        # tokenid = str(uuid.uuid3(uuid.NAMESPACE_OID, str(result['username'])))
        username = result['username']
        redis_template.setex(tokenid, 3600, username)
        # msg = json.dumps(result, encoding='utf-8')
        # redis_template.hset('users', username, msg)
        result['tokenid'] = tokenid
        if "password" in result:
            del result['password']
        if "salt" in result:
            del result['salt']
        return {'status': error.SUCCESS_OK, 'msg': result}
    else:
        return {'status': error.FAILT_EROOR_PASSWORD, 'msg': "FAILT_EROOR_PASSWORD"}


def login(username, password):
    """
    用户登录逻辑
    :param username: 客户端传来的用户名，字符串类型
    :param password: 客户端传来的密码，字符串类型
    :return:
    """
    res = {}
    user_row = redis_template.hget('users', username)
    if user_row:
        user = json.loads(user_row, encoding='utf-8')
        return verify_password(user, password)

    response, result = sql_server.query_user(username)
    if response == 1:
        result['create_time'] = str(result['create_time'])
        msg = json.dumps(result, encoding='utf-8')
        redis_template.hset('users', username, msg)
        return verify_password(result, password)
    else:
        return {'status': error.FAILT_NO_ACCOUNT, 'msg': "FAILT_NO_ACCOUNT"}


def valid_token(tokenid):
    """
    验证token有效性，这里简单依据用户token是否过期来判断
    :param tokenid:
    :return: token对应的用户名
    """
    username = redis_template.get(tokenid)
    return username


def get_userinfo(username):
    """
    从缓存中获取用户的信息，没有就查询数据库，然后更新缓存
    :param username:
    :return: 返回用户信息
    """
    user_msg = redis_template.hget('users', username)
    if not user_msg:
        _, user = sql_server.query_user(username)
        if not user:
            return ''
        user['create_time'] = str(user['create_time'])
        # del user['password']
        # del user_msg['username']
        # msg = json.dumps(user_msg, encoding='utf-8')
        user_msg = json.dumps(user, encoding='utf-8')
        redis_template.hset('users', username, user_msg)
    return user_msg


def get_user_msg(tokenid):
    """
    根据token来获取用户信息
    :param tokenid: 用户token，字符串类型
    :return: 返回状态码和信息
    """
    username = valid_token(tokenid)
    if not username:
        return {'status': error.FAILD_TOKEN_INVALID, 'msg': "FAILD_TOKEN_INVALID"}
    user_msg = get_userinfo(username)
    if not user_msg:
        return {'status': error.FAILD_USER_NOTEXIST, 'msg': "FAILD_USER_NOTEXIST"}
    return {'status': error.SUCCESS_OK, 'msg': user_msg}


def show_profile(tokenid):
    """
    根据用户token查询用户信息
    :param tokenid: 用户token
    :return:
    """
    user_msg = get_user_msg(tokenid)
    if user_msg['status'] != error.SUCCESS_OK:
        return user_msg

    return user_msg


def update_nickname(tokenid, nickname):
    """
    根据用户的token，更新数据库中的用户昵称
    :param tokenid: 用户token
    :param nickname: 待更改的昵称内容
    :return:
    """
    user_msg = get_user_msg(tokenid)
    if user_msg['status'] != error.SUCCESS_OK:
        return user_msg

    user = json.loads(user_msg['msg'], encoding='utf-8')
    response = sql_server.update_nickname(user['username'], nickname)
    user['nickname'] = nickname
    redis_template.hset('users', user['username'], json.dumps(user, encoding='utf-8'))
    return {'status': error.SUCCESS_OK, 'msg': user}


def update_picture(tokenid, user_picture):
    """
    根据用户的token，更新数据库中的用户头像地址
    :param tokenid:
    :param user_picture:
    :return:
    """
    user_msg = get_user_msg(tokenid)
    if user_msg['status'] != error.SUCCESS_OK:
        return user_msg
    user = json.loads(user_msg['msg'], 'utf-8')
    response = sql_server.update_picture(user['username'], user_picture)
    user['user_picture'] = user_picture
    redis_template.hset('users', user['username'], json.dumps(user, encoding='utf-8'))
    return {'status': error.SUCCESS_OK, 'msg': user}


def logout(tokenid):
    """
    根据用户的token，完成用户登出
    :param tokenid:
    :return:
    """
    username = valid_token(tokenid)
    if not username:
        return {'status': error.FAILD_TOKEN_INVALID, 'msg': "FAILD_TOKEN_INVALID"}
    # 清楚用户缓存
    redis_template.delete(tokenid)
    return {'status': error.SUCCESS_OK, 'msg': 'SUCCESS_OK'}


def add(a, b, c=10):
    s = a + b + c
    return s
