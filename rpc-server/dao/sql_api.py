# -*- coding: utf-8 -*-

import pymysql
from dbutils.pooled_db import PooledDB
import datetime
from conf import conf


class WebSystemSql(object):
    """
    sql调用类,负责连接数据库，执行业务sql
    """
    def __init__(self):
        self.__mysql_pool = PooledDB(pymysql,
                                     mincached=conf.DB_MINCACHE,
                                     maxcached=conf.DB_MAXCACHE,
                                     maxconnections=conf.DB_MAXCONNECT,
                                     maxusage=conf.DB_MAXUSEAGE,
                                     blocking=conf.DB_BLOCKING,
                                     host=conf.DB_HOST,
                                     port=conf.DB_PORT,
                                     user=conf.DB_USER,
                                     passwd=conf.DB_PASS,
                                     db=conf.DB_DATABASE,
                                     use_unicode=False,
                                     charset="utf8mb4",
                                     cursorclass=pymysql.cursors.DictCursor, )

    def query_user(self, username):
        """
        根据用户名查询用户
        :param username:
        :return: 成功返回用户信息以及查询到的条数，失败返回none
        """
        response = None
        result = None
        with self.__mysql_pool.connection() as conn:
            with conn.cursor() as cursor:
                sql = "SELECT * FROM `users` WHERE `username`=%s"
                try:
                    response = cursor.execute(sql, (username,))
                    result = cursor.fetchone()
                except Exception as e:
                    print(e)
        return response, result

    def query_byid(self, userid):
        """
        根据用户id查询用户数据
        :param userid:
        :return:
        """
        response = None
        result = None
        with self.__mysql_pool.connection() as conn:
            with conn.cursor() as cursor:
                sql = "SELECT * FROM `users` WHERE `id`=%s"
                try:
                    response = cursor.execute(sql, (userid,))
                    result = cursor.fetchone()
                except Exception as e:
                    print(e)
        return response, result

    def insert_user(self, username, nickname, password):
        """
        插入新用户 todo
        :param username:
        :param nickname:
        :param password:
        :return:
        """
        response = None
        with self.__mysql_pool.connection() as conn:
            with conn.cursor() as cursor:
                sql = "INSERT INTO `users` (`username`, `nickname`, `password`, `create_time`) VALUES (%s, %s, %s)"
                try:
                    response = cursor.execute(sql, (username, nickname, password, datetime.datetime.now()))
                except Exception as e:
                    print(e)
            conn.commit()
        return response

    def update_picture(self, username, user_picture):
        """
        更新用户头像地址
        :param username:
        :param user_picture:
        :return:
        """
        response = None
        with self.__mysql_pool.connection() as conn:
            with conn.cursor() as cursor:
                sql = "UPDATE `users` SET `user_picture`=%s WHERE username=%s"
                try:
                    response = cursor.execute(sql, (user_picture, username))
                except Exception as e:
                    print(e)
            conn.commit()
        return response

    def update_nickname(self, username, nickname):
        """
        更新用户昵称
        :param username:
        :param nickname:
        :return:
        """
        response = None
        with self.__mysql_pool.connection() as conn:
            with conn.cursor() as cursor:
                sql = "UPDATE `users` SET `nickname`=%s WHERE username=%s"
                try:
                    response = cursor.execute(sql, (nickname, username))
                except Exception as e:
                    print(e)
            conn.commit()
        return response
