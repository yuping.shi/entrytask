# encoding=utf-8
"""
提供redis连接池
"""
import redis

pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
redis_template = redis.Redis(connection_pool=pool)
