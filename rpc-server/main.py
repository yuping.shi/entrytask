# coding: utf-8
import conf.conf
from conf import conf
from service.server import login, add, update_nickname, update_picture, logout, show_profile
from common.rpc_server import rpc_server

if __name__ == '__main__':
    s = rpc_server()
    s.register_method(login)  # 注册方法
    s.register_method(logout)
    s.register_method(show_profile)
    s.register_method(update_nickname)
    s.register_method(update_picture)
    # s.register_method(add)
    s.connect(conf.RPC_SVR_IP, conf.RPC_SVR_PORT)  # 传入要监听的端口
