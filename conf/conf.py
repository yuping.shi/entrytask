# coding=utf-8


# rpc配置
RPC_SVR_IP = "localhost"
RPC_SVR_PORT = 10010


# redis 配置
RDS_HOST = "localhost"
RDS_PORT = 6379

# mysql 配置
DB_HOST = "localhost"
DB_USER = "root"
DB_PASS = ""
DB_DATABASE = "websystem"
DB_PORT = 3306
DB_MINCACHE = 30
DB_MAXCACHE = 100
DB_MAXCONNECT = 249
DB_BLOCKING = False
DB_MAXUSEAGE = 10000


