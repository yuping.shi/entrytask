import multiprocessing

bind = 'localhost:9000'
workers = multiprocessing.cpu_count() * 2 + 1
threads = 3
backlog = 2048
worker_class = "gevent"
worker_connections = 1000
daemon = False
debug = True
proc_name = 'gunicorn_demo'
pidfile = './log/gunicorn.pid'
errorlog = './log/gunicorn.log'
