"""web-server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from biz import login, user_profile

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    url(r'^$', login.index),
    url(r'^login/$', login.login),
    # url(r'^image/(P?.*)/$', image_route.image_route),
    url(r'^user_profile/$', user_profile.user_profile),
    url(r'^show_profile/$', user_profile.show_profile),
    url(r'^update_user_picture/$', user_profile.user_picture_update),
    url(r'^update_nickname/$', user_profile.update_nickname),
    url(r'^logout/$', user_profile.logout)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + staticfiles_urlpatterns()
