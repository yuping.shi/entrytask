# encoding=utf-8

from common.rpc_client_pool import RpcClientPool
from conf import conf
from conf.log import log
import time

client_pool = RpcClientPool(conf.RPC_SVR_IP, conf.RPC_SVR_PORT)


def rpc_requests(func, *args):
    """
    隐藏rpc连接池的调用过程，如果出现异常，采取等待重试的方法
    :param func:
    :param args:
    :return:
    """
    client = client_pool.get_rpc_client()
    start = time.time()
    # reconn_count = 1
    # while reconn_count:
    #     try:
    #         result = client.call(func, *args)
    #         return result
    #     except Exception as e:
    #         log.info("socket error:error_type = %s | func = %s|args= %s" % (e, func, str(args)))
    #         # time.sleep(5)
    #     finally:
    #         client_pool.release_rpc_client(client)
    #     reconn_count -= 1

    try:
        result = client.call(func, *args)
        return result
    except Exception as e:
        log.info("socket error:error_type = %s | func = %s|args= %s" % (e, func, str(args)))
    finally:
        client_pool.release_rpc_client(client)
