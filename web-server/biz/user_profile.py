# -*- coding: utf-8 -*-
import json

from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.http import JsonResponse

from .. import settings
import os
from rpc_caller import rpc_requests
from error import error


def user_profile(request):
    return render(request, 'user.html')


def show_profile(request):
    """
    显示用户信息
    :param request:
    :return:
    """
    # 获取token，调用rpc服务
    res = {}
    res = rpc_requests('show_profile', request.COOKIES['tokenid'])

    # 根据rpc调用结果构造返回给前端的信息
    if res and 'status' in res:
        if res['status'] == 100:
            msg = json.loads(res['msg'], encoding='utf-8')
            return JsonResponse({'status': error.SUCCESS_OK, 'username': msg['username'], 'nickname': msg['nickname'],
                                 'picture': msg['user_picture']})
        else:
            return JsonResponse(res)
    else:
        return JsonResponse({'status': error.ERROR_SERVER_RESPONSE, 'msg': 'ERROR_SERVER_RESPONSE'})


def user_picture_update(request):
    """
    更新用户图片
    :param request:
    :return:
    """
    # 参数检查
    if not request.FILES or not request.FILES['picture']:
        return JsonResponse({'status': error.FAILT_NO_PICTURE, 'msg': 'FAILT_NO_PICTURE'})

    name_suffix = os.path.splitext(request.FILES['picture'].name.encode('utf-8'))[-1]
    picture_name = request.COOKIES['tokenid'].encode('utf-8') + name_suffix
    save_path = os.path.join(settings.MEDIA_ROOT, picture_name)
    relative_path = os.path.join(settings.IMG_DIR, picture_name)

    # 调用rpc服务
    res = {}
    res = rpc_requests('update_picture', request.COOKIES['tokenid'], relative_path)

    # 根据rpc调用结果构造返回给前端的信息
    if res and 'status' in res:
        if res['status'] == 100:
            # 保存用户头像到本地磁盘
            picture = request.FILES['picture']
            with open(save_path, 'wb') as f:
                for c in picture.chunks():
                    f.write(c)
            return JsonResponse({'status': error.SUCCESS_OK, 'picture': relative_path}, safe=False)
        else:
            return JsonResponse(res)
    else:
        return JsonResponse({'status': error.ERROR_SERVER_RESPONSE, 'msg': 'ERROR_SERVER_RESPONSE'})


def update_nickname(request):
    request.encoding = 'utf-8'
    newname = ''
    if 'nickname' in request.POST:
        newname = request.POST.get('nickname')
    else:
        body = json.loads(request.body, encoding='utf-8')
        newname = body['nickname']
    # print request.COOKIES['tokenid']

    # 调用rpc服务
    res = rpc_requests('update_nickname', request.COOKIES['tokenid'], newname)
    # print("res", res)
    # 根据rpc调用结果构造返回给前端的信息
    if res and 'status' in res:
        if res['status'] == 100:
            return JsonResponse({'status': error.SUCCESS_OK, 'nickname': newname}, safe=False)
        else:
            return JsonResponse(res)
    else:
        return JsonResponse({'status': error.ERROR_SERVER_RESPONSE, 'msg': 'ERROR_SERVER_RESPONSE'})


def logout(request):
    request.encoding = 'utf-8'
    res = {}

    # 调用rpc服务
    res = rpc_requests('logout', request.COOKIES['tokenid'])

    # 根据rpc调用结果构造返回给前端的信息
    if res and 'status' in res:
        if res['status'] == 100:
            resp = JsonResponse(res)
            resp.delete_cookie(key='tokenid')
            return resp
        else:
            return JsonResponse(res)
    else:
        return JsonResponse({'status': error.ERROR_SERVER_RESPONSE, 'msg': 'ERROR_SERVER_RESPONSE'})
