#!/usr/bin/python
# -*- coding: UTF-8 -*-
import json

from django.shortcuts import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, redirect
from rpc_caller import rpc_requests
from error import error


def index(request):
    return render(request, 'login.html')


def login(request):
    """
    用户登录逻辑，
    :param request:
    :return:
    """
    # 从请求中获取用户名和密码
    request.encoding = 'utf-8'
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
    else:
        body = json.loads(request.body, encoding='utf-8')
        username = body['username']
        password = body['password']

    # 参数校验
    if len(username) > 64 or len(password) > 32:
        return JsonResponse({'status': error.ERROR_PARAMETER, 'msg': 'ERROR_PARAMETER'})

    # rpc调用
    res = rpc_requests("login", username, password)
    # 根据返回结果返回数据给前端
    if res and 'status' in res:
        if res['status'] == 100:
            # response = render(request, 'user.html',
            #                   {'username': res['msg']['username'], 'nickname': res['msg']['nickname'],
            #                    'picture': res['msg']['user_picture']})
            response = JsonResponse({'status': error.SUCCESS_OK, 'msg': '登录成功'})
            response.set_cookie(key='tokenid', value=res['msg']['tokenid'])
            return response
        else:
            return JsonResponse(res)
    return JsonResponse({'status': error.ERROR_SERVER_RESPONSE, 'msg': 'ERROR_SERVER_RESPONSE'})
